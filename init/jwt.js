const USERS = require('../conf/auth_users.js')
var JWT   = require('jsonwebtoken');
const secret = "NeverShareYourSecret"
const apps = require('../apps');

// JWT validation function
var validate = function (decoded, request, callback) {
    // do your checks to see if the person is valid
    if (!USERS[decoded.userName]) {
      return callback(null, false);
    }
    else {
      return callback(null, true);
    }
};

// JWT Support
// To generate token: curl http://localhost:9999/jwt/generate
// To test token validity: curl http://localhost:9999/jwt/me -H 'Authorization: <token>'
exports.register = function(server) {
	// JWT routes support
	server.register(require('hapi-auth-jwt2'), function (err) {
		if(err){
			console.log(err);
		}
		server.auth.strategy('jwt', 'jwt', {
			key: secret,
			validateFunc: validate,
			verifyOptions: { algorithms: [ 'HS256' ] }
	    });

	    server.auth.default('jwt');

	    server.route([
	    {
	        method: "GET", path: "/jwt/generate", config: { auth: false },
	        handler: function(request, reply) {
	        	const obj = USERS["crossid"]
				const token = JWT.sign(obj, secret);
				reply({token});
			}
		},
		{
			method: 'GET', path: '/jwt/me', config: { auth: 'jwt' },
	        handler: function(request, reply) {
				reply(request.auth.credentials)
				.header("Authorization", request.headers.authorization);
			}
		}
		]);

	    // TODO: Temporary until we figure out how to wait for inits to complete before registring the handlers
		Object.keys(apps).forEach(appName => apps[appName].register(server))
	});
}