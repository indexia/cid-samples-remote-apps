# Preface

The purpose of this project is to serve multiple variations of demo apps to test integration with CrossID product.

# Auth

Some of the applications are JWT protected, to generate a JWT, run:

```bash
curl http://www.indexia.co:9999/jwt/generate
```

to validate an existing JWT, run:

```bash
curl http://www.indexia.co:9999/jwt/me -H 'Authorization: <token>'
```

# Applications

## HR

Demonstrate a human resources app with support for JSON & XML formats.

## JRSG - JSON REST Single Group

JSON application where users can be members of single group only.

Listing all users:

```bash
curl -H 'Authorization: <token>' http://www.indexia.co:9999/jrsg/users'
```


## JRMG - JSON REST Multi Group

JSON application where users can be member of multiple groups.

