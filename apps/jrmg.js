const utils = require('../utils/types')

let USERS = [
	{
		userName: "asaf",
		firstName: 'Asaf',
		lastName: 'Shakarzy',
		status: 1,
		groups: [
			{
				"_id": 1,
				"name": "Admins"
			},
			{
				"_id": 2,
				"name": "Members"
			},
			{
				"_id": 3,
				"name": "Guests"
			}
		],
		profile: 1
	},
	{
		userName: "erezs",
		firstName: 'Erez',
		lastName: 'Sharim',
		status: 1,
		groups: [
			{
				"_id": 2,
				"name": "Members"
			},
			{
				"_id": 3,
				"name": "Guests"
			}
		],
		profile: 3
	},
	{
		userName: "benjaminf",
		firstName: 'Benjamin',
		lastName: 'Franklin',
		status: 0,
		groups: [
			{
				"_id": 3,
				"name": "Guests"
			}
		],
		profile: 2
	}
]

const ORIGINAL_USERS = utils.deepClone(USERS)

let GROUPS = [
	{
		"_id": 1,
		"name": "Admins"
	},
	{
		"_id": 2,
		"name": "Members"
	},
	{
		"_id": 3,
		"name": "Guests"
	}
]

const ORIGINAL_GROUPS = utils.deepClone(GROUPS)

let PROFILES = [
	{
		"_id": 1,
		"name": "CEO"
	},
	{
		"_id": 2,
		"name": "IT Supervisor"
	},
	{
		"_id": 3,
		"name": "System Architect"
	}
]

const ORIGINAL_PROFILES = utils.deepClone(PROFILES)


// JSON App REST single group API
exports.register = function(server) {
	// reset data to its original state
	server.route({
	    method: 'POST',
	    path:'/jrmg/.reset',
	    handler: function (request, reply) {
	        USERS = utils.deepClone(ORIGINAL_USERS)
	        GROUPS = utils.deepClone(ORIGINAL_GROUPS)
	        PROFILES = utils.deepClone(ORIGINAL_PROFILES)
	        return reply({})
	    },
	    config: { auth: 'jwt' }
	})

	// simulates listing users with group membership returned as apart of the user's payload.
	// note: this is not always the situation, some rest APIs require extra calls to retrieve references such as groups
	server.route({
	    method: 'GET',
	    path:'/jrmg/users',
	    handler: function (request, reply) {
	        return reply(USERS)
	    },
	    config: { auth: 'jwt' }
	})

	// get user by user name
	server.route({
	    method: 'GET',
	    path:'/jrmg/users/{userName}',
	    handler: function (request, reply) {
			const { userName } = request.params
			const user = USERS.filter((user) => user.userName === userName)[0]

			if (!user) {
				return reply({}).code(404)
			}

	        return reply(user)
	    },
	    config: { auth: 'jwt' }
	})

	// create new user
	server.route({
		method: 'POST',
		path: '/jrmg/users',
		handler: function (request, reply) {
			const { payload } = request
			// check if user already exist
			const user = USERS.filter((user) => user.userName === payload.userName)[0]
			// conflict
			if (user) {
				return reply({}).code(409)
			}
			// ok
			USERS.push(payload)
	        return reply(payload).code(201)
	    },
		config: { auth: 'jwt' }
	})

	// replace user
	server.route({
		method: 'PUT',
		path: '/jrmg/users/{userName}',
		handler: function (request, reply) {
			const { payload, params: { userName } } = request
			// check that user exit
			const user = USERS.filter((user) => user.userName === userName)[0]

			if (!user) {
				return reply({}).code(404)
			}

			USERS = USERS.filter((user) => user.userName !== userName)
			USERS.push(payload)
	        return reply(payload)
	    },
		config: { auth: 'jwt' }
	})

	// delete user
	server.route({
		method: 'DELETE',
		path: '/jrmg/users/{userName}',
		handler: function (request, reply) {
			const { userName } = request.params

			// check that user exit
			const user = USERS.filter((user) => user.userName === userName)[0]
			if (!user) {
				return reply({}).code(404)
			}

			USERS = USERS.filter((user) => user.userName !== userName)
	        return reply()
	    },
		config: { auth: 'jwt' }
	})

	// simulates listing groups without members.
	server.route({
	    method: 'GET',
	    path:'/jrmg/groups',
	    handler: function (request, reply) {
	        return reply(GROUPS)
	    },
	    config: { auth: 'jwt' }
	})

	// simulates listing groups without members.
	server.route({
	    method: 'GET',
	    path:'/jrmg/profiles',
	    handler: function (request, reply) {
	        return reply(PROFILES)
	    },
	    config: { auth: 'jwt' }
	})
}
