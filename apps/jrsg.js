const xml = require("js2xmlparser");

let USERS = [
	{
		name: "Asaf Shakarzy",
		userName: "asaf_sh",
		firstName: 'Asaf',
		lastName: 'Shakarzy',
		status: 1,
		permissionGroup: {
			"id": 1,
			"name": "Admins"
		}
	},
	{
		name: "Erez Sharim",
		userName: "erez_sh",
		firstName: 'Erez',
		lastName: 'Sharim',
		status: 0,
		permissionGroup: {
			"id": 2,
			"name": "Members"
		}
	},
	{
		name: "Benjamin Franklin",
		userName: "benjamin_f",
		firstName: 'Benjamin',
		lastName: 'Franklin',
		status: 1,
		permissionGroup: {
			"id": 3,
			"name": "Guests"
		}
	}
]

const GROUPS = [
	{
		"id": 1,
		"name": "Admins"
	},
	{
		"id": 2,
		"name": "Members"
	},
	{
		"id": 3,
		"name": "Guests"
	}
]

// JSON App REST single group API
exports.register = function(server) {
	server.route({
	    method: 'GET',
	    path:'/jrsg/users',
	    handler: function (request, reply) {
	        return reply(USERS);
	    },
	    config: { auth: 'jwt' }
	});

	server.route({
	    method: 'GET',
	    path:'/jrsg/users/{userName}',
	    handler: function (request, reply) {
			const { userName } = request.params;
			const user = USERS.filter((user) => user.userName === userName)[0];
	        return reply(user);
	    },
	    config: { auth: 'jwt' }
	});

	server.route({
		method: 'POST',
		path: '/jrsg/users',
		handler: function (request, reply) {
			const { payload } = request;
			USERS.push(payload)
	        return reply(payload);
	    },
		config: { auth: 'jwt' }
	});

	server.route({
		method: 'PUT',
		path: '/jrsg/users/{userName}',
		handler: function (request, reply) {
			const { payload, params: { userName } } = request;
			USERS = USERS.filter((user) => user.userName !== userName);
			USERS.push(payload)
	        return reply(payload);
	    },
		config: { auth: 'jwt' }
	});

	server.route({
		method: 'DELETE',
		path: '/jrsg/users/{userName}',
		handler: function (request, reply) {
			const { userName } = request.params;
			USERS = USERS.filter((user) => user.userName !== userName);
	        return reply();
	    },
		config: { auth: 'jwt' }
	});

	server.route({
	    method: 'GET',
	    path:'/jrsg/groups',
	    handler: function (request, reply) {
	        return reply(GROUPS);
	    },
	    config: { auth: 'jwt' }
	});
}
