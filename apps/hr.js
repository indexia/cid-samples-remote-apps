const xml = require("js2xmlparser");

const EMPLOYEES = [
{
			EmployeeID: 20001,
			First_Name: 'Asaf',
			Last_Name: 'Shakarzy',
			Status: 2,
			Devision: 800,
			Location: 100,
			PositionLevel: 'SVP',
			EmployeeType: {
				"@": {
					"i:nil": true
				}
			},
			StartDate: '0001-01-01T00:00:00',
			TerminationDate: '0001-01-01T00:00:00',
			Positions: {
				Position: [
					{
						PositionID: 'P-105100',
						ReportsToPosition: 'P-105888',
						Factor: 1
					}
				]
			}
		},
		{
			EmployeeID: 20002,
			First_Name: 'Erez',
			Last_Name: 'Sharim',
			Status: 2,
			Devision: 801,
			Location: 101,
			PositionLevel: 'SVP',
			StartDate: '2010-03-21T00:00:00',
			TerminationDate: '0001-01-01T00:00:00',
			Positions: {
				Position: [
					{
						PositionID: 'P-205102',
						ReportsToPosition: 'P-305883',
						Factor: 1
					}
				]
			}
		}
]

const xmlArr = {
	"@": {
		"xmlns": "http://schemas.datacontract.oprg/2004/07/IDMService",
		"xmlns:i": "http://www.w3.org/2001/XMLSchema-instance"
	},
	"Employee": [EMPLOYEES]
}

exports.register = function(server) {
	// HR App with JSON
	server.route({
	    method: 'GET',
	    path:'/hr/json', 
	    handler: function (request, reply) {
	        return reply(EMPLOYEES);
	    },
	    config: { auth: false }
	});

 	// HR App with XML
	server.route({
	    method: 'GET',
	    path:'/hr/xml', 
	    handler: function (request, reply) {
	        return reply(xml.parse("ArrayOfEmployee", xmlArr, {})).header("Content-Type", "application/xml")
	    },
	    config: { auth: false }
	});
}
