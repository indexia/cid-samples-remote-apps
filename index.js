const Hapi = require('hapi');
const apps = require('./apps');
const inits = require('./init');

// Create a server with a host and port
const server = new Hapi.Server();
server.connection({ 
    host: '0.0.0.0', 
    port: 9999
});
Object.keys(inits).forEach(name => inits[name].register(server))
// TODO: Temporary until we figure out how to wait for inits to complete before registring the handlers
// Object.keys(apps).forEach(appName => apps[appName].register(server))

// Start the server
server.start((err) => {
    if (err) {
        throw err;
    }
    console.log('Server running at:', server.info.uri);
});